﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System.IO;

namespace CannadianTire.Core
{
    class WebDriverFactory
    {
        private static IWebDriver driver;
  
        public static IWebDriver CreateDriver(Browser browserName)
        {
            switch (browserName) {
                case Browser.Chrome:
                    driver = new ChromeDriver();
                    break;
                case Browser.Firefox:
                    driver =  new FirefoxDriver();
                    break;
                case Browser.InternetExplorer:
                    driver =  new InternetExplorerDriver();
                    break;
            }
            return driver;
        }

        public static void CloseDriver()
        {
            if (driver != null)
            {
                driver.Quit();
            }
        }
    }
}

