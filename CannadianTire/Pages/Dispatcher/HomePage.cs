﻿using CannadianTire.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannadianTire.Pages
{
    public class HomePage : BasePage
    {
        public const String SIGN_IN_BUTTON_XPATH = "//button[contains(@title,'Sign In')]";
        public const String MY_ACCOUNT_DESC_BUTTON_XPATH = "//button[contains(@title,'My Account desc')]";
        public const String SIGN_IN_MENU_BUTTON_XPATH = "//a[@data-component='MenuButton']/span[text()='Sign in']";
        public const String SIGN_OUT_MENU_BUTTON_XPATH = "//a[@data-component='MenuButton']/span[text()='Sign out']";


        [FindsBy(How = How.XPath, Using = MY_ACCOUNT_DESC_BUTTON_XPATH)]
        private IWebElement myAccountDescButtom;

        [FindsBy(How = How.XPath, Using = SIGN_IN_BUTTON_XPATH)]
        private IWebElement signInButton;

        [FindsBy(How = How.XPath, Using = SIGN_IN_MENU_BUTTON_XPATH)]
        private IWebElement signInMenuButton;

        [FindsBy(How = How.XPath, Using = SIGN_OUT_MENU_BUTTON_XPATH)]
        private IWebElement signOutMenuButton;

        public HomePage(IWebDriver driver)
            : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public HomePage ClickOnSignInButton() 
        {
            ClickUtil.ClickOnWebElement(signInButton, driver);
            return this;
        }

        public SignInPage ClickOnSignInMenuButton()
        {
            ClickUtil.ClickOnWebElement(signInMenuButton, driver);
            return new SignInPage(driver);
        }

        public HomePage ClickOnMyAccountDescButtom()
        {
            ClickUtil.ClickOnWebElement(myAccountDescButtom, driver);
            return this;
        }

        public HomePage VerifySignOutIsDisplayed()
        {
            Assert.IsTrue(signOutMenuButton.Displayed);
            return this;
        } 
    }
}
