﻿using CannadianTire.Helper;
using CannadianTire.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannadianTire.Pages
{
    public class SignInPage : BasePage
    {
        private const String LOGIN_INPUT_XPATH = "//input[@id='email']";
        private const String PASSWORD_INPUT_XPATH = "//input[@id='password']";
        private const String SIGN_IN_BUTTON_XPATH = "//button[@type='submit']";

        [FindsBy(How = How.XPath, Using = LOGIN_INPUT_XPATH)]
        private IWebElement loginInput;

        [FindsBy(How = How.XPath, Using = PASSWORD_INPUT_XPATH)]
        private IWebElement passwordInput;

        [FindsBy(How = How.XPath, Using = SIGN_IN_BUTTON_XPATH)]
        private IWebElement signInButton;

        public SignInPage(IWebDriver driver)
            : base(driver) 
        {
            PageFactory.InitElements(driver, this);
        }

        public SignInPage TypeToEmailInput(String email)
        {
            WaitUtil.WaitElementForPresent(loginInput, driver);
            loginInput.Clear();
            loginInput.SendKeys(email);
            WaitUtil.WaitTextToBePresentInElementValue(By.XPath(LOGIN_INPUT_XPATH), email, driver);
            return this;
        }

        public SignInPage TypeToPasswordInput(String password)
        {
            WaitUtil.WaitElementForPresent(passwordInput, driver);
            passwordInput.Clear();
            passwordInput.SendKeys(password);
            WaitUtil.WaitTextToBePresentInElementValue(By.XPath(PASSWORD_INPUT_XPATH), password, driver);
            return this;
        }

        public HomePage ClickOnSignInButton()
        {
            WaitUtil.WaitElementForPresent(signInButton, driver);
            ClickUtil.ClickOnWebElement(signInButton, driver);
            return new HomePage(driver);
        }
    }
}
