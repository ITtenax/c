﻿using CannadianTire.Tests;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace CannadianTire
{
    [TestFixture]
    public class SignInTest : BaseTests
    {
        [Test]
        public void LoginTest()
        {
            signInPage = homePage.ClickOnSignInButton().ClickOnSignInMenuButton();
            homePage = signInPage
               .TypeToEmailInput(ConfigurationSettings.AppSettings["userLogin"])
               .TypeToPasswordInput(ConfigurationSettings.AppSettings["userPass"])
               .ClickOnSignInButton();
            homePage.ClickOnMyAccountDescButtom();
            homePage.VerifySignOutIsDisplayed();
        }
    }
}
