﻿using CannadianTire.Core;
using CannadianTire.Pages;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using NUnit.Framework;

namespace CannadianTire.Tests
{
    [SetUpFixture]
    public class BaseTests
    {
        protected IWebDriver Driver;
        protected HomePage homePage;
        protected SignInPage signInPage;

        [SetUp]
        public void SetUp() {
            Driver = WebDriverFactory.CreateDriver(Browser.Chrome);
            Driver.Manage().Window.Maximize();
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(Double.Parse(ConfigurationSettings.AppSettings["implicityWait"])));
            Driver.Navigate().GoToUrl(ConfigurationSettings.AppSettings["homePageUrl"]);
            homePage = new HomePage(Driver);
        }


        [TearDown]
        public void TearDown() {
            WebDriverFactory.CloseDriver();
        }

    }
}
