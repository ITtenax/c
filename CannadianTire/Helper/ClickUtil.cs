﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannadianTire.Helper
{
    public class ClickUtil
    {
        public static void ClickOnWebElement(IWebElement element, IWebDriver driver) {
            WaitUtil.WaitElementForPresent(element, driver);
            element.Click();
        }
    }
}
