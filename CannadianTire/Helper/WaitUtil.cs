﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannadianTire.Helper
{
    class WaitUtil
    {
        public static void WaitElementForPresent(IWebElement element, IWebDriver driver) 
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementToBeClickable(element));
        }
        public static void WaitTextToBePresentInElementValue(By element, String text, IWebDriver driver)
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.TextToBePresentInElementValue(element, text));
        }
    }
}
